const HDWalletProvider = require("@truffle/hdwallet-provider");

const mnemonic = 'wire bullet menu exact make spell cabin will step abuse panel armed'
module.exports = {
//    contracts_build_directory: "./output",
    networks: {
        ropsten: {
          provider: function() {
            return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/v3/0d6c797dafe5405f8470c2056635b039");
          },
          network_id: '3',
        }
    },
    compilers: {
      solc: {
        version: "^0.4.17"
      }
    }
};
